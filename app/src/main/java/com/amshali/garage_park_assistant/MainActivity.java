package com.amshali.garage_park_assistant;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.GenericJson;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

	static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	static final JsonFactory JSON_FACTORY = new JacksonFactory();
	private TextView mTextView;
	private Handler mHandler = new Handler();
	private Runnable mHandlerTask = new Runnable() {
		@Override
		public void run() {
			new PollDuration().execute("");
		}
	};
	private State state = State.UNKNOWN;
	private SharedPreferences sharedPreferences;

	enum State {
		FORWARD,
		G_BACK,
		GOOD,
		UNKNOWN
	}

	private class PollDuration extends AsyncTask<String, Void, String> {
		int duration = -1;

		@Override
		protected String doInBackground(String... args) {
			// params comes from the execute() call: params[0] is the url.
			HttpRequestFactory requestFactory =
					HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
						@Override
						public void initialize(HttpRequest request) {
							request.setParser(new JsonObjectParser(JSON_FACTORY));
						}
					});
			try {
				String deviceId = sharedPreferences.getString("device_id", "");
				String accessToken = sharedPreferences.getString("access_token", "");
				String url = "https://api.particle.io/v1/devices/" + deviceId + "/duration?access_token=" + accessToken;
				HttpRequest request = requestFactory.buildGetRequest(new GenericUrl(url));
				request.setConnectTimeout(20000);
				request.setReadTimeout(20000);
				GenericJson data = request.execute().parseAs(GenericJson.class);
				duration = Integer.parseInt(data.get("result").toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}


		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			int cm = (int) (duration * (340.29 / 20000));
			int min = Integer.parseInt(sharedPreferences.getString("min_distance_to_wall", "80"));
			int max = Integer.parseInt(sharedPreferences.getString("max_distance_from_wall", "95"));
			int refreshRate = Integer.parseInt(sharedPreferences.getString("refresh_rate", "750"));
			if (cm < 10 || cm > 600 && state != State.UNKNOWN) {
				mTextView.setText(Integer.toString(cm));
				mTextView.setTextColor(Color.WHITE);
				mTextView.setBackgroundColor(Color.DKGRAY);
				state = State.UNKNOWN;
			} else if (cm > min && cm < max && state != State.GOOD) {
				mTextView.setText(R.string.good_state);
				mTextView.setTextColor(Color.BLACK);
				mTextView.setBackgroundColor(Color.GREEN);
				state = State.GOOD;
			} else if (cm < min && state != State.G_BACK) {
				mTextView.setText(R.string.go_back_state);
				mTextView.setTextColor(Color.WHITE);
				mTextView.setBackgroundColor(Color.RED);
				state = State.G_BACK;
			} else if (cm > max && state != State.FORWARD) {
				mTextView.setText(R.string.forward_state);
				mTextView.setTextColor(Color.BLACK);
				mTextView.setBackgroundColor(Color.YELLOW);
				state = State.FORWARD;
			}
			mHandler.postDelayed(mHandlerTask, refreshRate);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);


		mTextView = (TextView) findViewById(R.id.textView);
		mTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 150);
		mTextView.setTextColor(Color.WHITE);
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		mHandler.post(mHandlerTask);
	}

	@Override
	protected void onStop() {
		super.onStop();
		mHandler.removeCallbacks(mHandlerTask);
	}

	private static final String TAG = MainActivity.class.getSimpleName();

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_settings:
				Intent i = new Intent(this, SettingsActivity.class);
				startActivity(i);
				break;
			default:
				return super.onOptionsItemSelected(item);
		}

		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

}
