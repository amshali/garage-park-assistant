# Garage park assistant
Garage park assistant will help you park your car in your house's garage with appropriate distance from the wall and garage's door.

This android app is setup to read the duration measured by a photon device (https://www.particle.io/) which is equipped with an ultrasound sonar sensor. 

This device is used in a garage to signal a driver during their parking action. It will tell a driver to come forward or go back or to stop when the distance from the wall is good enough. The message is shown on the screen of a mobile phone or tablet which can be installed inside the garage. 

## Architecture
This applciation is comprised of two parts.
- A device to measure the duration of a traveling sound signal from the object (e.g. a car)
- An android app which reads the duration and translate that into an action (for the driver).

Here is a schematic of connections between Photon and HC SR04 ultra sound sonar used in this application:

```
                    +---------------------------------+
                    |                                 |
                +---------------------------------+   |
                |   |                             |   |
           +---------------------------------+    |   |
           |    |   |                        |    |   |
      +----------------------------------+   |    |   |
      |    |    |   |                    |   |    |   |
   +--v----v----v---v---------+          |   |    |   |
   |  GND  VCC  D2  D6        |      +---+---+----+---+----+
   |                          |      |  GND VCC TRIG ECHO  |
   |      Photon              |      |                     |
   |                          |      |   HC SR04           |
   |                          |      +---------------------+
   +--------------------------+         +----+    +----+
                                          |         ^
                                          |         |
                                   Signal |         | Echo
                                          |         |
                                          v         +

```

The photon is connected to particle.io cloud(https://api.particle.io/v1/devices) and publishes the value of a variable named `duration` every 750 ms. See the code for the photon device in `/photon/` directory.

The android app located in `/app/` directory polls the value of the variable `duration` every 900 ms and translate that into an action. The value of this variable is first converted into a distance. If distance is
- less than a minimum value, the action will be _Go Back_, 
- more than a maximum value, the action will be _Forward_,
- between the minimum and maximum, the action will be _Good :)_(Otherwise _Stop_).

## Parts

- HC SR04 ![HC SR04](/hcsr04.jpg?raw=true "The ultra sound sonar")
- Photon from particle.io ![Photon](/photon-plugged-in.jpg?raw=true "Photon from particle.io")
- A breadboard
- Wires
- Micro USB charger cable x 2
- A 1.5" x 3" x 2" box to package the photon and sonar
- A tablet or unused android phone

## Deployment
I have deployed this system in my garage. Here is a photo of system in action:
![Garage park assistant in action](/deployment.jpg?raw=true "Garage park assistant in action")

At the top you can see an android device installed on the wall and at the bottom you see a packaged photon and HC SR04 device. 
